
// Mad Lib
// Kathy and Brett

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>
using namespace std;

void Print(string Array[], ostream &out = cout);

int main()
{

	const int NUM_WORDS = 10;
	string answers[NUM_WORDS] = { "adj", "adj2", "noun1", "noun2", "plnoun1", "game", "plnoun2", "verb1", "verb2", "adj3" };

	string questions[NUM_WORDS] = {
		"n adjective (describing word): ",
		"n adjective (describing word): ",
		" noun (place): ",
		" noun (thing): ",
		" noun (plural): ",
		" game: ",
		" noun (plural): ",
		" verb (ending in -ing): ",
		" verb (ending in -ing): ",
		"n adjective (describing word): "
	};

	for (int i = 0; i < NUM_WORDS; i++)
	{
		cout << "Enter a" << questions[i];
		getline(cin, answers[i]);
	}

	Print(answers);

	char input = 'n';
	cout << "\nSave to file? (y/n): ";
	cin >> input;

	if (input == 'y' || input == 'Y')
	{
		ofstream ofs("C:\\temp\\mad.txt");
		Print(answers, ofs);
		ofs.close();
	}

	_getch();
	return 0;
}

void Print(string Array[], ostream &out)
{


	out << "\nA vacation is when you take a trip to some " << Array[0]
		<< " place with your " << Array[1] << " family.\nUsually you go to some place that is near a/an "
		<< Array[2] << " or up on a/an " << Array[3] << ".\nA good vacation place is one where you can ride "
		<< Array[4] << " or play " << Array[5] << " or go hunting for " << Array[6] << ".\nI like to spend my time "
		<< Array[7] << " or " << Array[8] << ".\nParents need vacations more than kids because parents are always very "
		<< Array[9] << ".";
}